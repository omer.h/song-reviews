import { Component, OnInit } from '@angular/core';
import { artist } from 'src/assets/DM/artist';
import { song } from 'src/assets/DM/song';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public topTenArtist:artist[]=[];
  public topTenSongs:song[]=[];

  constructor() { 
    for(let i=0;i<10;i++){
      this.topTenArtist.push(new artist());
    }
    for(let i=0;i<10;i++){
      this.topTenSongs.push(new song());
    }
  }

  ngOnInit(): void {
  }

}
