import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArtistComponent } from './artist/artist.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { SongsComponent } from './songs/songs.component';
import{HomePageComponent} from './home-page/home-page.component';
import{UsersComponent} from './users/users.component';
import{SearchComponent} from './search/search.component';

const routes: Routes = [
  { path: 'Song', component: SongsComponent },
  { path: 'Users', component: UsersComponent },
  { path: 'Artist', component: ArtistComponent },
  { path: 'HomePage', component: HomePageComponent },
  { path: 'Search', component: SearchComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
