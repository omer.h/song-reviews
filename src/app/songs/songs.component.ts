import { Component, OnInit } from '@angular/core';
import { song } from 'src/assets/DM/song';
import { reviews } from 'src/assets/DM/reviews';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {

  public songs:song[]=[];
  public reviews: reviews[]=[] ;
  

  constructor() {
    for(let i=0;i<5;i++){
      this.reviews.push(new reviews());
    }
    for(let i=0;i<10;i++){
      this.songs.push(new song());
    }
   }

  ngOnInit(): void {
  }

}
