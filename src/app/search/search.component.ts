import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {  Input  } from '@angular/core';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public chooseSearchObject:boolean=false;
  public optionArray:string[]=["Songs","Artists","Users","Reviews"];
  isReviews:boolean=false;
  isUsers:boolean=false;
  isSongs:boolean=false;
  isArtist:boolean=false;
  @Output() public search = new EventEmitter<any>();
  // @Output() public show = new EventEmitter<any>();
  // @Output() public show = new EventEmitter<any>();
  public songRec=new FormGroup({
    selectedValue:new FormControl(''),
    songName: new FormControl('', Validators.required),
    artistName: new FormControl('', Validators.required),
    reviews:new FormControl('',Validators.required),
    numOfFolowers:new FormControl('',Validators.required),
    rate:new FormControl('',Validators.required),
    numOfComments:new FormControl('',Validators.required),
    users:new FormControl('',Validators.required),
    date:new FormControl('',Validators.required)

  });
  public buttonDisabled: boolean =false; 

  constructor() {this.buildForm(); }

  ngOnInit(): void {
    this.buildForm();
  }
  advenceSearch(){

  }

  buildForm(){
    this.songRec=new FormGroup
    ({
      selectedValue:new FormControl(''),
        songName    : new FormControl('',Validators.required),
        artistName  : new FormControl('',Validators.required),
        reviews:new FormControl('',Validators.required),
        numOfFolowers:new FormControl('',Validators.required),
        rate:new FormControl('',Validators.required),
        numOfComments:new FormControl('',Validators.required),
        users:new FormControl('',Validators.required),
        date:new FormControl('',Validators.required)
      }
    )
  }
  
  onSearch(){
    // debugger;
    this.buttonDisabled = true;
    this.songRec.get('selectedValue');
    if(this.songRec.get('selectedValue')?.value!=''){
      this.chooseSearchObject=true;
    }
    if(this.songRec.get('selectedValue')?.value!='Songs'){
      this.isSongs=true;
    }
    else if(this.songRec.get('selectedValue')?.value!='Artists'){
      this.isArtist=true;
    }
    else if(this.songRec.get('selectedValue')?.value!='Users'){
      this.isUsers=true;
    }
    else if(this.songRec.get('selectedValue')?.value!='Reviews'){
      this.isReviews=true;
    }
    
    this.search.emit(this.songRec.value);
    // this.sleep(6000)
    // this.buttonDisabled = false;
    console.log(this.songRec.value)
  }

  get songName(): any {
    return this.songRec.get('songName');
  }

  get artistName() :any {
    return this.songRec.get('artistName');
  }
  get reviews(): any {
    return this.songRec.get('reviews');
  }

  get numOfFolowers() :any {
    return this.songRec.get('numOfFolowers');
  }
  get selectedValue() :any {
    return this.songRec.get('selectedValue');
  }
  get numOfComments(): any {
    return this.songRec.get('numOfComments');
  }

  get users() :any {
    return this.songRec.get('users');
  }
  get date(): any {
    return this.songRec.get('date');
  }

  get rate() :any {
    return this.songRec.get('rate');
  }
  // sleep(ms) {
  //   return new Promise(resolve => setTimeout(resolve, ms));
  // }

}
